package ru.panasyuk.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.panasyuk.tm.client.TaskRestEndpointClient;
import ru.panasyuk.tm.client.TasksRestEndpointClient;
import ru.panasyuk.tm.marker.IntegrationCategory;
import ru.panasyuk.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    final TasksRestEndpointClient tasksClient = TasksRestEndpointClient.client();

    @NotNull
    final List<Task> tasks = new ArrayList<>();

    @Before
    public void init() {
        for (int i = 0; i < 4; i++) {
            @NotNull final Task task = new Task("Task " + i);
            tasks.add(task);
            client.save(task);
        }
    }

    @After
    public void afterTest() {
        for (@NotNull final Task task : tasks) {
            client.delete(task);
        }
        tasks.clear();
    }

    @Test
    public void testSave() {
        final long taskSize = tasksClient.count();
        final long expectedSize = taskSize + 1;
        @NotNull final Task taskTest = new Task("Task Test");
        @Nullable final Task savedTask = client.save(taskTest);
        Assert.assertNotNull(savedTask);
        final long actualSize = tasksClient.count();
        Assert.assertEquals(expectedSize, actualSize);
        client.delete(taskTest);
    }

    @Test
    public void testFindById() {
        for (@NotNull final Task task : tasks) {
            @Nullable final Task foundTask = client.findById(task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void testDeleteById() {
        for (@NotNull final Task task : tasks) {
            client.deleteById(task.getId());
            @Nullable final Task deletedTask = client.findById(task.getId());
            Assert.assertNull(deletedTask);
        }
    }

    @Test
    public void testDelete() {
        for (@NotNull final Task task : tasks) {
            client.delete(task);
            @Nullable final Task deletedTask = client.findById(task.getId());
            Assert.assertNull(deletedTask);
        }
    }

}