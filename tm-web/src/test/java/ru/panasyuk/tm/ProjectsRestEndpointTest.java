package ru.panasyuk.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.panasyuk.tm.client.ProjectRestEndpointClient;
import ru.panasyuk.tm.client.ProjectsRestEndpointClient;
import ru.panasyuk.tm.marker.IntegrationCategory;
import ru.panasyuk.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectsRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    final ProjectsRestEndpointClient projectsClient = ProjectsRestEndpointClient.client();

    @NotNull
    final List<Project> projects = new ArrayList<>();

    @Before
    public void init() {
        for (int i = 0; i < 4; i++) {
            @NotNull final Project project = new Project("Project " + i);
            projects.add(project);
            client.save(project);
        }
    }

    @After
    public void afterTest() {
        for (@NotNull final Project project : projects) {
            client.delete(project);
        }
        projects.clear();
    }

    @Test
    public void testCount() {
        final long projectSize = projectsClient.count();
        final long expectedSize = projectSize + 1;
        @NotNull final Project project = new Project("Project Test");
        client.save(project);
        final long actualSize = projectsClient.count();
        Assert.assertEquals(expectedSize, actualSize);
        client.delete(project);
    }

    @Test
    public void testDeleteAll() {
        final long projectSize = projectsClient.count();
        Assert.assertTrue(projectSize > 0);
        projectsClient.deleteAll();
        final long newProjectSize = projectsClient.count();
        Assert.assertEquals(0, newProjectSize);
    }

    @Test
    public void testFindAll() {
        final long projectSize = projectsClient.count();
        @Nullable final List<Project> foundProjects = projectsClient.findAll();
        Assert.assertNotNull(foundProjects);
        Assert.assertEquals(foundProjects.size(), projectSize);
    }

    @Test
    public void testSaveAll() {
        final long projectSize = projectsClient.count();
        final long expectedSize = projectSize + 2;
        @NotNull final Project projectTest1 = new Project("Project Test 1");
        @NotNull final Project projectTest2 = new Project("Project Test 2");
        @NotNull final List<Project> testProjects = new ArrayList<>();
        testProjects.add(projectTest1);
        testProjects.add(projectTest2);
        projectsClient.saveAll(testProjects);
        final long actualSize = projectsClient.count();
        Assert.assertEquals(expectedSize, actualSize);
        client.delete(projectTest1);
        client.delete(projectTest2);
    }

}