package ru.panasyuk.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.panasyuk.tm.client.TaskRestEndpointClient;
import ru.panasyuk.tm.client.TasksRestEndpointClient;
import ru.panasyuk.tm.marker.IntegrationCategory;
import ru.panasyuk.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public class TasksRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    final TasksRestEndpointClient tasksClient = TasksRestEndpointClient.client();

    @NotNull
    final List<Task> tasks = new ArrayList<>();

    @Before
    public void init() {
        for (int i = 0; i < 4; i++) {
            @NotNull final Task task = new Task("Task " + i);
            tasks.add(task);
            client.save(task);
        }
    }

    @After
    public void afterTest() {
        for (@NotNull final Task task : tasks) {
            client.delete(task);
        }
        tasks.clear();
    }

    @Test
    public void testCount() {
        final long taskSize = tasksClient.count();
        final long expectedSize = taskSize + 1;
        @NotNull final Task task = new Task("Task Test");
        client.save(task);
        final long actualSize = tasksClient.count();
        Assert.assertEquals(expectedSize, actualSize);
        client.delete(task);
    }

    @Test
    public void testDeleteAll() {
        final long taskSize = tasksClient.count();
        Assert.assertTrue(taskSize > 0);
        tasksClient.deleteAll();
        final long newTaskSize = tasksClient.count();
        Assert.assertEquals(0, newTaskSize);
    }

    @Test
    public void testFindAll() {
        final long taskSize = tasksClient.count();
        @Nullable final List<Task> foundTasks = tasksClient.findAll();
        Assert.assertNotNull(foundTasks);
        Assert.assertEquals(foundTasks.size(), taskSize);
    }

    @Test
    public void testSaveAll() {
        final long taskSize = tasksClient.count();
        final long expectedSize = taskSize + 2;
        @NotNull final Task taskTest1 = new Task("Task Test 1");
        @NotNull final Task taskTest2 = new Task("Task Test 2");
        @NotNull final List<Task> testTasks = new ArrayList<>();
        testTasks.add(taskTest1);
        testTasks.add(taskTest2);
        tasksClient.saveAll(testTasks);
        final long actualSize = tasksClient.count();
        Assert.assertEquals(expectedSize, actualSize);
        client.delete(taskTest1);
        client.delete(taskTest2);
    }

}