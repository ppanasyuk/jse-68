package ru.panasyuk.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.panasyuk.tm.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {
}