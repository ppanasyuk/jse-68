package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.panasyuk.tm.api.service.IProjectService;
import ru.panasyuk.tm.dto.soap.*;
import ru.panasyuk.tm.model.Project;

@Endpoint
public class ProjectSoapEndpointImpl {

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public static final String NAMESPACE = "http://panasyuk.ru/tm/dto/soap";

    @Autowired
    private IProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    private ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        projectService.clear();
        return new ProjectClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    private ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) {
        return new ProjectCountResponse(projectService.count());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    private ProjectDeleteAllResponse deleteAll(@RequestPayload final ProjectDeleteAllRequest request) {
        projectService.clear(request.getProjects());
        return new ProjectDeleteAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    private ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.deleteById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    private ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        projectService.delete(request.getProject());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectExistByIdRequest", namespace = NAMESPACE)
    private ProjectExistByIdResponse existById(@RequestPayload final ProjectExistByIdRequest request) {
        return new ProjectExistByIdResponse(projectService.existById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    private ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        return new ProjectFindAllResponse(projectService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        return new ProjectFindByIdResponse(projectService.findById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        @NotNull Project project = new Project();
        project.setId(request.getProject().getId());
        project.setName(request.getProject().getName());
        project.setDescription(request.getProject().getDescription());
        project.setStatus(request.getProject().getStatus());
        project.setDateStart(request.getProject().getDateStart());
        project.setDateFinish(request.getProject().getDateFinish());
        projectService.save(project);
        return new ProjectSaveResponse(project);
    }

}