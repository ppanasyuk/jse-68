package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.tm.api.endpoint.IProjectEndpoint;
import ru.panasyuk.tm.api.service.IProjectService;
import ru.panasyuk.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Nullable
    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @NotNull
    @PostMapping(value = "/save", produces = "application/json")
    public Project save(@RequestBody @NotNull final Project project) {
        return projectService.save(project);
    }

    @Override
    @PostMapping("/saveAll")
    public void saveAll(
            @RequestBody @NotNull final List<Project> projects
    ) {
        projectService.saveAll(projects);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") @NotNull final String id) {
        return projectService.findById(id);
    }

    @Override
    @GetMapping("/existById/{id}")
    public boolean existById(@PathVariable("id") @NotNull final String id) {
        return projectService.existById(id);
    }

    @Override
    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count() {
        return projectService.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) {
        projectService.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final Project project) {
        projectService.delete(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(@RequestBody @NotNull final List<Project> projects) {
        projectService.clear(projects);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        projectService.clear();
    }

}