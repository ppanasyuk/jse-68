package ru.panasyuk.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.panasyuk.tm.api.service.ITaskService;
import ru.panasyuk.tm.dto.soap.*;
import ru.panasyuk.tm.model.Task;

@Endpoint
public class TaskSoapEndpointImpl {

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public static final String NAMESPACE = "http://panasyuk.ru/tm/dto/soap";

    @Autowired
    private ITaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    private TaskClearResponse clear(@RequestPayload final TaskClearRequest request) {
        taskService.clear();
        return new TaskClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    private TaskCountResponse count(@RequestPayload final TaskCountRequest request) {
        return new TaskCountResponse(taskService.count());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    private TaskDeleteAllResponse deleteAll(@RequestPayload final TaskDeleteAllRequest request) {
        taskService.clear(request.getTasks());
        return new TaskDeleteAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    private TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.deleteById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    private TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) {
        taskService.delete(request.getTask());
        return new TaskDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskExistByIdRequest", namespace = NAMESPACE)
    private TaskExistByIdResponse existById(@RequestPayload final TaskExistByIdRequest request) {
        return new TaskExistByIdResponse(taskService.existById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    private TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        return new TaskFindAllResponse(taskService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        return new TaskFindByIdResponse(taskService.findById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        Task task = new Task();
        task.setId(request.getTask().getId());
        task.setName(request.getTask().getName());
        task.setDescription(request.getTask().getDescription());
        task.setStatus(request.getTask().getStatus());
        task.setDateStart(request.getTask().getDateStart());
        task.setDateFinish(request.getTask().getDateFinish());
        task.setProjectId(request.getTask().getProjectId());
        taskService.save(task);
        return new TaskSaveResponse(request.getTask());
    }

}