package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.tm.api.endpoint.ITaskEndpoint;
import ru.panasyuk.tm.api.service.ITaskService;
import ru.panasyuk.tm.model.Task;

import javax.jws.WebMethod;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Nullable
    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @NotNull
    @PostMapping(value = "/save", produces = "application/json")
    public Task save(@RequestBody @NotNull final Task task) {
        return taskService.save(task);
    }

    @Override
    @PostMapping("/saveAll")
    public void saveAll(@RequestBody @NotNull final List<Task> tasks) {
        taskService.saveAll(tasks);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") @NotNull final String id) {
        return taskService.findById(id);
    }

    @Override
    @GetMapping("/existById/{id}")
    public boolean existById(@PathVariable("id") @NotNull final String id) {
        return taskService.existById(id);
    }

    @Override
    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count() {
        return taskService.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) {
        taskService.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final Task task) {
        taskService.delete(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(@RequestBody @NotNull final List<Task> tasks) {
        taskService.clear(tasks);
    }

    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        taskService.clear();
    }

}