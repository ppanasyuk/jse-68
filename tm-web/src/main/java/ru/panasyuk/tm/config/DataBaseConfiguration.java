package ru.panasyuk.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.panasyuk.tm.api.IPropertyService;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.panasyuk.tm")
@EnableJpaRepositories("ru.panasyuk.tm.repository")
public class DataBaseConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDBDriver());
        dataSource.setUrl(propertyService.getDBUrl());
        dataSource.setUsername(propertyService.getDBUser());
        dataSource.setPassword(propertyService.getDBPassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.panasyuk.tm.model");
        @NotNull final Map<String, String> settings = new HashMap<>();
        @NotNull final String dialect = propertyService.getDBDialect();
        @NotNull final String hbm2DdlAuto = propertyService.getDBHbm2DdlAuto();
        @NotNull final Boolean showSql = propertyService.getDBLoggingEnabled();
        @NotNull final String lazyLoadNoTransEnabled = propertyService.getDBLazyLoadNoTransEnabled();
        @NotNull final String formatSql = propertyService.getDBFormatSql();
        @NotNull final String secondLevelCache = propertyService.getDBSecondLevelCacheEnabled();
        @NotNull final String cacheRegionFactory = propertyService.getDBCacheRegionFactory();
        @NotNull final String useQueryCache = propertyService.getDBUseQueryCache();
        @NotNull final String useMinimalPuts = propertyService.getDBUseMinimalPuts();
        @NotNull final String cacheRegionPrefix = propertyService.getDBCacheRegionPrefix();
        @NotNull final String configFilePath = propertyService.getDBConfigFilePath();
        settings.put(Environment.DIALECT, dialect);
        settings.put(Environment.HBM2DDL_AUTO, hbm2DdlAuto);
        settings.put(Environment.SHOW_SQL, showSql.toString());
        settings.put(Environment.ENABLE_LAZY_LOAD_NO_TRANS, lazyLoadNoTransEnabled);
        settings.put(Environment.FORMAT_SQL, formatSql);
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, secondLevelCache);
        settings.put(Environment.CACHE_REGION_FACTORY, cacheRegionFactory);
        settings.put(Environment.USE_QUERY_CACHE, useQueryCache);
        settings.put(Environment.USE_MINIMAL_PUTS, useMinimalPuts);
        settings.put(Environment.CACHE_REGION_PREFIX, cacheRegionPrefix);
        settings.put(Environment.CACHE_PROVIDER_CONFIG, configFilePath);
        factoryBean.setJpaPropertyMap(settings);
        return factoryBean;
    }

}