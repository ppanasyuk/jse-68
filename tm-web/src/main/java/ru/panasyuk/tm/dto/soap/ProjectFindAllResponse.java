package ru.panasyuk.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.panasyuk.tm.model.Project;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectFindAllResponse")
public class ProjectFindAllResponse {

    private List<Project> projects;

    public ProjectFindAllResponse(final List<Project> projects) {
        this.projects = projects;
    }

}