package ru.panasyuk.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.panasyuk.tm.model.Project;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectSaveResponse")
public class ProjectSaveResponse {

    private Project project;

    public ProjectSaveResponse(final Project project) {
        this.project = project;
    }

}