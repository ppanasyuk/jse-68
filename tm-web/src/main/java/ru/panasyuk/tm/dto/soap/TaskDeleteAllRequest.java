package ru.panasyuk.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.panasyuk.tm.model.Task;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskDeleteAllRequest")
public class TaskDeleteAllRequest {

    private List<Task> tasks;

    public TaskDeleteAllRequest(final List<Task> tasks) {
        this.tasks = tasks;
    }

}