package ru.panasyuk.tm.dto.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "status")
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private static String displayNames = "";

    public static String getDisplayNames() {
        if (!Status.displayNames.isEmpty()) return Status.displayNames;
        @NotNull final StringBuilder displayNames = new StringBuilder();
        int index = 0;
        for (@NotNull final Status status : values()) {
            displayNames.append(status.getDisplayName());
            if (index < values().length - 1) displayNames.append(", ");
            index++;
        }
        Status.displayNames = "[" + displayNames + "]";
        return Status.displayNames;
    }

    @Nullable
    public static String toName(@Nullable final Status status) {
        if (status == null) return null;
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Status status : values()) {
            if (status.getDisplayName().equals(value))
                return status;
        }
        return null;
    }

    @NotNull
    public final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}