package ru.panasyuk.tm.dto.soap;

import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public ProjectClearRequest createProjectClearRequest() {
        return new ProjectClearRequest();
    }

    public ProjectClearResponse createProjectProjectClearResponse() {
        return new ProjectClearResponse();
    }

    public ProjectCountRequest createProjectCountRequest() {
        return new ProjectCountRequest();
    }

    public ProjectCountResponse createProjectCountResponse() {
        return new ProjectCountResponse();
    }

    public ProjectDeleteAllRequest createProjectDeleteAllRequest() {
        return new ProjectDeleteAllRequest();
    }

    public ProjectDeleteAllResponse createProjectDeleteAllResponse() {
        return new ProjectDeleteAllResponse();
    }

    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    public ProjectExistByIdResponse createProjectExistByIdResponse() {
        return new ProjectExistByIdResponse();
    }

    public ProjectExistByIdRequest createProjectExistByIdRequest() {
        return new ProjectExistByIdRequest();
    }

    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    public TaskClearRequest createTaskClearRequest() {
        return new TaskClearRequest();
    }

    public TaskClearResponse createTaskTaskClearResponse() {
        return new TaskClearResponse();
    }

    public TaskCountRequest createTaskCountRequest() {
        return new TaskCountRequest();
    }

    public TaskCountResponse createTaskCountResponse() {
        return new TaskCountResponse();
    }

    public TaskDeleteAllRequest createTaskDeleteAllRequest() {
        return new TaskDeleteAllRequest();
    }

    public TaskDeleteAllResponse createTaskDeleteAllResponse() {
        return new TaskDeleteAllResponse();
    }

    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    public TaskExistByIdResponse createTaskExistByIdResponse() {
        return new TaskExistByIdResponse();
    }

    public TaskExistByIdRequest createTaskExistByIdRequest() {
        return new TaskExistByIdRequest();
    }

    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

    public TaskFindAllRequest createTaskFindAllRequest() {
        return new TaskFindAllRequest();
    }

    public TaskFindAllResponse createTaskFindAllResponse() {
        return new TaskFindAllResponse();
    }

    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

}