package ru.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.panasyuk.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    List<Task> findAll();

    @NotNull
    @WebMethod
    Task save(
            @WebParam(name = "task", partName = "task")
            @NotNull Task task
    );

    @WebMethod
    void saveAll(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull List<Task> tasks
    );

    @Nullable
    @WebMethod
    Task findById(
            @WebParam(name = "id", partName = "id")
            @NotNull String id
    );

    @WebMethod
    boolean existById(
            @WebParam(name = "id", partName = "id")
            @NotNull String id
    );

    @WebMethod
    long count();

    @WebMethod
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull String id
    );

    @WebMethod
    void delete(
            @WebParam(name = "task", partName = "task")
            @NotNull Task task
    );

    @WebMethod
    void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull List<Task> tasks
    );

    @WebMethod
    void clear();

}