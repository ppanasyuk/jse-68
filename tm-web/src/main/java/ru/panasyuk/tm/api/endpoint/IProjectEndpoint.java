package ru.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.panasyuk.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    List<Project> findAll();

    @NotNull
    @WebMethod
    Project save(
            @WebParam(name = "project", partName = "project")
            @NotNull Project project
    );

    @WebMethod
    public void saveAll(
            @WebParam(name = "projects", partName = "projects")
            @NotNull List<Project> projects
    );

    @Nullable
    @WebMethod
    Project findById(
            @WebParam(name = "id", partName = "id")
            @NotNull String id
    );

    @WebMethod
    boolean existById(
            @WebParam(name = "id", partName = "id")
            @NotNull String id
    );

    @WebMethod
    long count();

    @WebMethod
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull String id
    );

    @WebMethod
    void delete(
            @WebParam(name = "project", partName = "project")
            @NotNull Project project
    );

    @WebMethod
    void deleteAll(
            @WebParam(name = "projects", partName = "projects")
            @NotNull List<Project> projects
    );

    @WebMethod
    void clear();

}