package ru.t1.panasyuk.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.UserDTO;

public interface IUserDtoRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

}