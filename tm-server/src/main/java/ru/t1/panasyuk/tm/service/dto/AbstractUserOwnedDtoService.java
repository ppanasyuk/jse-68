package ru.t1.panasyuk.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.panasyuk.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.panasyuk.tm.repository.dto.AbstractUserOwnedDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends AbstractUserOwnedDtoRepository<M>>
        implements IUserOwnedDtoService<M> {

    @Getter
    @NotNull
    @Autowired
    private R repository;

    @Override
    @Transactional
    public M add(@NotNull final String userId, @Nullable final M model) {
        if (model == null) return null;
        model.setUserId(userId);
        @NotNull final R repository = getRepository();
        repository.save(model);
        return model;
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final R repository = getRepository();
        repository.deleteAll();
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @Nullable final List<M> models;
        @NotNull final R repository = getRepository();
        models = repository.findAll();
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        @NotNull final R repository = getRepository();
        repository.deleteAll();
        repository.saveAll(models);
        return models;
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final R repository = getRepository();
        repository.save(model);
    }

}