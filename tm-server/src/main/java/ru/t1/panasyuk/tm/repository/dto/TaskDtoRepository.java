package ru.t1.panasyuk.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> {

    @Nullable
    @Query("FROM TaskDTO m WHERE m.userId = :userId ORDER BY m.created DESC")
    List<TaskDTO> findByIndex(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @NotNull
    List<TaskDTO> findByUserIdAndProjectIdOrderByCreatedDesc(@NotNull String userId, @Nullable String projectId);

    void deleteByUserIdAndProjectId(@NotNull String userId, @Nullable String projectId);

    long countByUserId(@NotNull String userId);

    void deleteByUserId(@NotNull String userId);

    @Nullable
    TaskDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<TaskDTO> findByUserId(@NotNull String userId, @NotNull Sort sort);

}