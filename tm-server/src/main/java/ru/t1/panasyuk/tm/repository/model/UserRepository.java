package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.model.User;

import java.util.List;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findFirstByLogin(@Nullable String login);

    @Nullable
    User findFirstByEmail(@Nullable String email);

    @Nullable
    @Query("FROM User m ORDER BY m.created DESC")
    List<User> findByIndex(@NotNull Pageable pageable);

}