package ru.t1.panasyuk.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.api.service.ILoggerService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.endpoint.AbstractEndpoint;
import ru.t1.panasyuk.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
@NoArgsConstructor
public final class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @NotNull
    @Autowired
    private Backup backup;

    private void exit() {
        System.exit(0);
    }

    private void initBackup() {
        backup.start();
    }

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.initJmsLogger();
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String port = getPropertyService().getServerPort();
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    public void start() {
        initPID();
        initEndpoints();
        initLogger();
    }

    private void prepareShutdown() {
        backup.stop();
        loggerService.info("*** TASK MANAGER IS SHUTTING DOWN ***");
    }

}