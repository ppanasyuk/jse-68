package ru.t1.panasyuk.tm.service;

import lombok.SneakyThrows;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.panasyuk.tm.api.service.ILoggerService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.listener.EntityListener;
import ru.t1.panasyuk.tm.listener.JmsLoggerProducer;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

@Service
public final class LoggerService implements ILoggerService, InitializingBean {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMANDS_FILE = "./commands.xml";

    @NotNull
    private static final String ERRORS = "ERROR";

    @NotNull
    private static final String ERRORS_FILE = "./errors.xml";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = "./messages.xml";

    @NotNull
    private static final LogManager MANAGER = LogManager.getLogManager();

    @NotNull
    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    @NotNull
    private static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);

    @NotNull
    private static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);

    @NotNull
    private static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);

    @NotNull
    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    @NotNull
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    private static void loadConfigFromFile() {
        try {
            @NotNull final Class<?> clazz = LoggerService.class;
            @Nullable final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE);
            MANAGER.readConfiguration(inputStream);
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @NotNull
    private static ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private static void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            if (!fileName.isEmpty())
                logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    @SneakyThrows
    public void initJmsLogger() {
        @NotNull final JmsLoggerProducer jmsLoggerProducer = new JmsLoggerProducer(propertyService);
        @NotNull final EntityListener entityListener = new EntityListener(jmsLoggerProducer);
        @NotNull final SessionFactoryImpl sessionFactoryImpl = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry eventListenerRegistry = sessionFactoryImpl.getServiceRegistry().getService(EventListenerRegistry.class);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

    @NotNull
    public static Logger getLoggerCommand() {
        return LOGGER_COMMAND;
    }

    @NotNull
    public static Logger getLoggerErrors() {
        return LOGGER_ERROR;
    }

    @NotNull
    public static Logger getLoggerMessage() {
        return LOGGER_MESSAGE;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        loadConfigFromFile();
        @NotNull final String log = propertyService.getApplicationLog();
        registry(LOGGER_COMMAND, log + COMMANDS_FILE, false);
        registry(LOGGER_ERROR, log + ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, log + MESSAGES_FILE, true);
    }

}