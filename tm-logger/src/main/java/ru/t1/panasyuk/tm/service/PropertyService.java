package ru.t1.panasyuk.tm.service;

import lombok.Cleanup;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.panasyuk.tm.api.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['activeMQ.host']}")
    private String activeMqHost;

    @Value("#{environment['activeMQ.port']}")
    private String activeMqPort;

    @Value("#{environment['application.config']}")
    private String applicationConfig;

    @Value("#{environment['mongo.client.host']}")
    private String mongoClientHost;

    @Value("#{environment['mongo.client.port']}")
    private String mongoClientPort;

}