package ru.t1.panasyuk.tm.endpoint;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.panasyuk.tm.api.endpoint.IAuthEndpoint;
import ru.t1.panasyuk.tm.api.endpoint.IUserEndpoint;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.request.user.*;
import ru.t1.panasyuk.tm.dto.response.user.UserLoginResponse;
import ru.t1.panasyuk.tm.dto.response.user.UserRegistryResponse;
import ru.t1.panasyuk.tm.dto.response.user.UserRemoveResponse;
import ru.t1.panasyuk.tm.dto.response.user.UserViewProfileResponse;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.marker.SoapCategory;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.service.PropertyService;

@Category(SoapCategory.class)
@DisplayName("Тестирование эндпоинта User")
public class UserEndpointTest {

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IUserEndpoint userEndpoint;

    @NotNull
    private static IPropertyService propertyService;

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private static String newUserToken;

    @Nullable
    private static String userPass = "TEST";

    @BeforeClass
    public static void initUser() {
        propertyService = new PropertyService();
        authEndpoint = IAuthEndpoint.newInstance(propertyService);
        userEndpoint = IUserEndpoint.newInstance(propertyService);
        @NotNull UserLoginRequest loginRequest = new UserLoginRequest("TEST_ADMIN", "TEST_ADMIN");
        @NotNull UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        adminToken = response.getToken();
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest("TEST", "TEST", "TEST@TEST");
        userEndpoint.registryUser(registryRequest);
        loginRequest = new UserLoginRequest("TEST", userPass);
        response = authEndpoint.loginUser(loginRequest);
        userToken = response.getToken();
    }

    @AfterClass
    public static void dropUser() {
        @NotNull UserLogoutRequest logoutRequest = new UserLogoutRequest(userToken);
        authEndpoint.logoutUser(logoutRequest);
        @NotNull UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, "TEST");
        userEndpoint.removeUser(removeRequest);
        if (newUserToken == null) return;
        removeRequest = new UserRemoveRequest(adminToken, "NEW");
        userEndpoint.removeUser(removeRequest);
    }

    @Test
    @DisplayName("Изменение пароля")
    public void changePasswordTest() {
        @NotNull final String newPass = "NEW_PASS";
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(userToken, newPass);
        Assert.assertNotNull(userEndpoint.changeUserPassword(request));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("TEST", newPass);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.loginUser(loginRequest);
        Assert.assertNotNull(loginResponse);
        userToken = loginResponse.getToken();
        Assert.assertNotNull(userToken);
        Assert.assertFalse(userToken.isEmpty());
        userPass = newPass;
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение пароля без токена")
    public void changePasswordTestNegative() {
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(null, "PASS");
        userEndpoint.changeUserPassword(request);
    }

    @Test
    @DisplayName("Блокирование и разблокирование пользователя")
    public void lockAndUnlockTest() {
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken, "TEST");
        Assert.assertNotNull(userEndpoint.lockUser(request));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("TEST", userPass);
        @NotNull final UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        Assert.assertFalse(response.getSuccess());
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken, "TEST");
        Assert.assertNotNull(userEndpoint.unlockUser(unlockRequest));
        @NotNull final UserLoginRequest newLoginRequest = new UserLoginRequest("TEST", userPass);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.loginUser(newLoginRequest);
        Assert.assertNotNull(loginResponse);
        userToken = loginResponse.getToken();
        Assert.assertNotNull(userToken);
        Assert.assertFalse(userToken.isEmpty());
    }

    @Test(expected = Exception.class)
    @DisplayName("Блокирование пользователя не администратором")
    public void lockTestNegative() {
        @NotNull final UserLockRequest request = new UserLockRequest(userToken, "SADMIN");
        userEndpoint.lockUser(request);
    }

    @Test(expected = Exception.class)
    @DisplayName("Разблокирование пользователя не администратором")
    public void unlockTestNegative() {
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(userToken, "SADMIN");
        userEndpoint.unlockUser(unlockRequest);
    }

    @Test
    @DisplayName("Выход пользователя")
    public void logoutTest() {
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(userToken);
        Assert.assertNotNull(authEndpoint.logoutUser(logoutRequest));
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(userToken, userPass);
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(changePasswordRequest));
        @NotNull final UserLoginRequest newLoginRequest = new UserLoginRequest("TEST", userPass);
        @Nullable final UserLoginResponse loginResponse = authEndpoint.loginUser(newLoginRequest);
        Assert.assertNotNull(loginResponse);
        userToken = loginResponse.getToken();
        Assert.assertNotNull(userToken);
        Assert.assertFalse(userToken.isEmpty());
    }

    @Test(expected = Exception.class)
    @DisplayName("Выход пользователя без токена")
    public void logoutTestNegative() {
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(null);
        authEndpoint.logoutUser(logoutRequest);
    }

    @Test
    @DisplayName("Регистрация и удаление пользователя")
    public void registryAndRemoveTest() {
        @NotNull final String newUserLogin = "NEW";
        @NotNull final String newUserPassword = "PASS";
        @NotNull final String newUserEmail = "NEW@NEW";
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest(newUserLogin, newUserPassword, newUserEmail);
        Assert.assertNotNull(userEndpoint.registryUser(registryRequest));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(newUserLogin, newUserPassword);
        @Nullable final UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        Assert.assertNotNull(response);
        newUserToken = response.getToken();
        Assert.assertNotNull(newUserToken);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(newUserToken);
        authEndpoint.logoutUser(logoutRequest);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, newUserLogin);
        @NotNull final UserRemoveResponse removeResponse = userEndpoint.removeUser(removeRequest);
        Assert.assertTrue(removeResponse.getSuccess());
        @NotNull final UserLoginRequest loginRemovedUserRequest = new UserLoginRequest(newUserLogin, newUserPassword);
        @NotNull final UserLoginResponse removedUserResponse = authEndpoint.loginUser(loginRemovedUserRequest);
        Assert.assertFalse(removedUserResponse.getSuccess());
        newUserToken = null;
    }

    @Test
    @DisplayName("Регистрация пользователя с уже существующим логином")
    public void registryLoginExistsNotSuccessTest() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest("SADMIN", "NEW", "TEST@TEST1");
        @NotNull final UserRegistryResponse response = userEndpoint.registryUser(registryRequest);
        Assert.assertFalse(response.getSuccess());
    }

    @Test
    @DisplayName("Регистрация пользователя с уже существующим Email")
    public void registryEmailExistsTestNegative() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest("NEW", "NEW", "TEST@TEST");
        @NotNull final UserRegistryResponse response = userEndpoint.registryUser(registryRequest);
        Assert.assertFalse(response.getSuccess());
    }

    @Test
    @DisplayName("Регистрация пользователя с пустым логином")
    public void registryEmptyLoginTestNegative() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest("", "NEW", "TEST@TEST");
        @NotNull final UserRegistryResponse response = userEndpoint.registryUser(registryRequest);
        Assert.assertFalse(response.getSuccess());
    }

    @Test
    @DisplayName("Регистрация пользователя с пустым паролем")
    public void registryEmptyPasswordTestNegative() {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest("NEW", "", "TEST@TEST");
        @NotNull final UserRegistryResponse response = userEndpoint.registryUser(registryRequest);
        Assert.assertFalse(response.getSuccess());
    }

    @Test(expected = Exception.class)
    @DisplayName("Удаление пользователя не администратором")
    public void removeTestNegative() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(userToken, "SADMIN");
        userEndpoint.removeUser(removeRequest);
    }

    @Test
    @DisplayName("Обновление профиля пользователя")
    public void updateProfileTest() {
        @NotNull final String newFirstName = "FIRST NAME";
        @NotNull final String newLastName = "LAST NAME";
        @NotNull final String newMiddleName = "MIDDLE NAME";
        @NotNull final UserUpdateProfileRequest updateRequest = new UserUpdateProfileRequest(userToken, newLastName, newFirstName, newMiddleName);
        Assert.assertNotNull(userEndpoint.updateUserProfile(updateRequest));
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(userToken);
        @Nullable final UserViewProfileResponse response = authEndpoint.viewUserProfile(viewRequest);
        Assert.assertNotNull(response);
        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());
    }

    @Test(expected = Exception.class)
    @DisplayName("Обновление профиля пользователя без токена")
    public void updateProfileTestNegative() {
        @NotNull final UserUpdateProfileRequest updateRequest = new UserUpdateProfileRequest(null, "FIRST", "LAST", "MIDDLE");
        userEndpoint.updateUserProfile(updateRequest);
    }

    @Test
    @DisplayName("Просмотр профиля пользователя")
    public void viewProfileTest() {
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(userToken);
        @Nullable final UserViewProfileResponse response = authEndpoint.viewUserProfile(viewRequest);
        Assert.assertNotNull(response);
        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user);
    }

    @Test(expected = Exception.class)
    @DisplayName("Просмотр профиля пользователя без токена")
    public void viewProfileTestNegative() {
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(null);
        authEndpoint.viewUserProfile(viewRequest);
    }

}