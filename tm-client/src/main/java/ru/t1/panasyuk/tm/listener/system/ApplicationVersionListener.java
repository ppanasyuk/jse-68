package ru.t1.panasyuk.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.system.ServerVersionRequest;
import ru.t1.panasyuk.tm.dto.response.system.ServerVersionResponse;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
@Qualifier("has-argument")
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Show version info.";

    @NotNull
    public static final String NAME = "version";

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        @NotNull final ServerVersionRequest request = new ServerVersionRequest();
        ServerVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}