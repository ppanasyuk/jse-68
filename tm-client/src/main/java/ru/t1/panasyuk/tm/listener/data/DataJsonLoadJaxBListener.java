package ru.t1.panasyuk.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataJsonLoadJaxBRequest;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class DataJsonLoadJaxBListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Load data from json file.";

    @NotNull
    private static final String NAME = "data-load-json-jaxb";


    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonLoadJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest(getToken());
        domainEndpoint.loadDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
